%define SYS_READ 0
%define SYS_WRITE 1

%define STDIN_CODE 0
%define STDOUT_CODE 1

%define EXIT_CODE 60

%define NEW_LINE '\n'
%define TAB_SYMBOL 9

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    push rax
    
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STDOUT_CODE
    syscall
    
    pop rax
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi	
    mov rdi, rsp
    call print_string
    pop rdi	
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    .start:
        push rax
        push rdx
        mov rax, rdi
        mov rdi, 10
        push 0x0
    .integer_to_string_convert:
    	xor rdx, rdx
    	div rdi
    	add rdx, '0'
    	push rdx
    	cmp rax, 0
    	je .print_numeric_value
    	jmp .integer_to_string_convert
    .print_numeric_value:
    	pop rdi
    	cmp rdi, 0x0
    	je .end
    	call print_char
    	jmp .print_numeric_value
    .end:
        pop rdx
        pop rax
    	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte[rdi]
    cmp al, byte[rsi]
    jne .various
    inc rdi
    inc rsi
    test al, al
    jne string_equals
    mov rax, 1
    ret
.various:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    .start:
        push rsi
        push rdi
        push rdx
        push 0x0
    .read:
        mov rsi, rsp
        mov rdi, STDIN_CODE
        mov rdx, 1
        mov rax, SYS_READ
        syscall
    .end:
        pop rax
        pop rdx
        pop rdi
        pop rsi
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	mov r9, rsi
	push rdi
	push rsi

.space_loop:
	call read_char
	cmp rax, TAB_SYMBOL
	je .space_loop
	cmp rax, NEW_LINE
	je .space_loop
	cmp rax, " "
	je .space_loop

	pop rsi
	pop rdi
	xor r8, r8
	
.symbol_loop:
	cmp rax, 0
	jz .break
	cmp rax, TAB_SYMBOL
	je .break
	cmp rax, NEW_LINE
	je .break
	cmp rax, " "
	je .break
	cmp r8, r9
	jae .over
	mov [rdi+r8], rax
	inc r8
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	jmp .symbol_loop

.break:
	mov rax, rdi
	mov rdx, r8
	ret

.over:
	xor rax, rax
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r8, 10
    xor rcx, rcx
    xor r9, r9
.loop:
    mov r9b, byte [rdi + rcx]
    cmp r9b, '0'
    jb .end
    cmp r9b, '9'
    ja .end
    mul r8
    sub r9b, '0'
    add rax, r9
    inc rcx
    jmp .loop
    ret
.end:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .neg
    call parse_uint
    jmp .end

    .neg:
        inc rdi
        call parse_uint
        cmp rdx, 0
        jz .end
        neg rax
        inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	call string_length
	inc rax
	cmp rax, rdx
	ja .error
	mov r8, rax
	dec rdi
	dec rsi
	xor rcx, rcx

.loop:
	cmp rcx, r8
	je .break
	inc rdi
	inc rsi
	mov r9b, byte[rdi]
	mov byte[rsi], r9b
	inc rcx
	jmp .loop

.break:
	mov rax, r8
	ret

.error:
	xor rax, rax
	ret
